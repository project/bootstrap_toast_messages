CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module replaces the standard drupal status messages and creates
Bootstrap 4 toast instead using javascript. This kind of messages are also known
as "Toast" notifications (or alerts).

Works for all status messages including messages created during Ajax requests.

This module is a perfect extension for the Drupal Commerce module.
It was first developed for a Drupal Commerce website.

** Note: Standard messages WILL SHOW for superuser (uid: 1).

REQUIREMENTS
------------
This module requires you to have Bootstrap 4 library installed and loaded.


INSTALLATION
------------
composer require drupal/bootstrap_toast_messages

Or, Install as normal (see http://drupal.org/documentation/install/modules-themes).


CONFIGURATION
-------------

* Navigate to Administration > Configuration > Bootstrap Toast Messages Settings
(admin/config/system/bootstrap-toast-messages/settings)

MAINTAINERS
-----------
* lexsoft - https://www.drupal.org/u/lexsoft
