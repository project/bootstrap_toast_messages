/**
 * @file bootstrap_toast_messages.js
 *
 * Javascript for integrating the bootstrap toast JS messages into drupal
 * Replaces the standard drupal status messages and creates bootstrap 4 toast instead
 */

(function ($, Drupal, once, drupalSettings) {

  'use strict';
  Drupal.bootstrap_toast_messages = {};
  // This is a unique string with format "drupal-[module name]"
  Drupal.bootstrapToastMessagesUnique = 'drupal-bootstrap-toast-messages';

  // The targeted element for the context to work in
  Drupal.bootstrapToastMessagesContext = 'body';

  $.toastDefaults = {
    position: 'top-right', /** top-left/top-right/top-center/bottom-left/bottom-right/bottom-center - Where the toast will show up **/
    dismissible: true, /** true/false - If you want to show the button to dismiss the toast manually **/
    stackable: true, /** true/false - If you want the toasts to be stackable **/
    pauseDelayOnHover: true, /** true/false - If you want to pause the delay of toast when hovering over the toast **/
    style: {
      toast: '', /** Classes you want to apply separated my a space to each created toast element (.toast) **/
      info: '', /** Classes you want to apply separated my a space to modify the "info" type style  **/
      success: '', /** Classes you want to apply separated my a space to modify the "success" type style  **/
      warning: '', /** Classes you want to apply separated my a space to modify the "warning" type style  **/
      error: '', /** Classes you want to apply separated my a space to modify the "error" type style  **/
    }
  };

  /**
   * Registers behaviours
   */
  Drupal.behaviors.bootstrap_toast_messages = {
    attach: function (context, settings) {

      const bootstrapToastMessagesUnique = once(Drupal.bootstrapToastMessagesUnique, Drupal.bootstrapToastMessagesContext, context);
      // Code inside this block runs once (only on initial page load)
      bootstrapToastMessagesUnique.forEach(function () {
        // Creating bootstrap toast messages on page request (html request)
        if (typeof settings.bootstrap_toast_messages !== 'undefined' && typeof settings.bootstrap_toast_messages.messages_array !== 'undefined') {
          Drupal.bootstrap_toast_messages.createMessages(settings.bootstrap_toast_messages.messages_array);
          settings.bootstrap_toast_messages.messages_array = null;
        }

      });

    }
  };

  // creating bootstrap toast messages on ajax requests
  Drupal.AjaxCommands.prototype.bootstrapToastMessagesCommand = function (ajax, response, status) {
    Drupal.bootstrap_toast_messages.createMessages(response.messages_array);
  };

  // Create and display bootstrap toast status messages
  Drupal.bootstrap_toast_messages.createMessages = function (messages_array) {

    // Defaults
    const type = 'status';
    const icon = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-info-circle" viewBox="0 0 16 16"><path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"/><path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0"/></svg>';
    const title = 'Info';
    const duration = drupalSettings.bootstrap_toast_messages.duration;


    if (Array.isArray(messages_array) && messages_array.length) {

      let _icon = icon;
      let _duration = duration;
      let _title = title;

      for (let i in messages_array) {
        if(messages_array[i].type === 'status' || messages_array[i].type === 'success') {
          messages_array[i].type = 'success';
          _icon = (messages_array[i].icon) ? messages_array[i].icon : '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#4ca154" class="bi bi-check2-circle mr-2" viewBox="0 0 16 16"><path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0"/><path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0z"/></svg>';
          _title = 'Info';
        }
        else if (messages_array[i].type === 'error' || messages_array[i].type === 'danger') {
          messages_array[i].type = 'error';
          _icon = (messages_array[i].icon) ? messages_array[i].icon : '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#ca3a31" class="bi bi-exclamation-octagon mr-2" viewBox="0 0 16 16"><path d="M4.54.146A.5.5 0 0 1 4.893 0h6.214a.5.5 0 0 1 .353.146l4.394 4.394a.5.5 0 0 1 .146.353v6.214a.5.5 0 0 1-.146.353l-4.394 4.394a.5.5 0 0 1-.353.146H4.893a.5.5 0 0 1-.353-.146L.146 11.46A.5.5 0 0 1 0 11.107V4.893a.5.5 0 0 1 .146-.353zM5.1 1 1 5.1v5.8L5.1 15h5.8l4.1-4.1V5.1L10.9 1z"/><path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0M7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0z"/></svg>';
          _duration = (messages_array[i].duration === undefined || messages_array[i].duration === null) ? 10000 : messages_array[i].duration;
          _title = 'Error';
        }
        else {
          messages_array[i].type = (messages_array[i].type!=='warning') ? type : messages_array[i].type;
          _icon = (messages_array[i].icon) ? messages_array[i].icon : '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#cc7c2e" class="bi bi-exclamation-triangle mr-2" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.15.15 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.2.2 0 0 1-.054.06.1.1 0 0 1-.066.017H1.146a.1.1 0 0 1-.066-.017.2.2 0 0 1-.054-.06.18.18 0 0 1 .002-.183L7.884 2.073a.15.15 0 0 1 .054-.057m1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0M7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0z"/></svg>';
          _duration = (messages_array[i].duration === undefined || messages_array[i].duration === null) ? 10000 : messages_array[i].duration;
          _title = 'Warning';
        }

        $.toast({
          type: messages_array[i].type,
          //title: _title,
          //subtitle: '11 mins ago',
          content: messages_array[i].message,
          delay: _duration,
          icon: _icon,
          // img: {
          //  src: 'https://via.placeholder.com/20',
          //  class: 'rounded-0', /**  Classes you want to apply separated my a space to modify the image **/
          //  alt: 'Image'
          //}
        });
      }

    }

  };

}(jQuery, Drupal, once, drupalSettings));
