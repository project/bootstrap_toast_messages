<?php

namespace Drupal\bootstrap_toast_messages\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command to create bootstrap toast messages.
 */
class BootstrapToastMessagesCommand implements CommandInterface {

  /**
   * The messages.
   *
   * @var array
   */
  protected $messagesArray;

  /**
   * {@inheritdoc}
   */
  public function __construct($messagesArray = NULL) {
    $this->messagesArray = $messagesArray;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'bootstrapToastMessagesCommand',
      'messages_array' => $this->messagesArray,
    ];
  }

}
