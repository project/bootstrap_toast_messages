<?php

namespace Drupal\bootstrap_toast_messages\EventSubscriber;

use Drupal\bootstrap_toast_messages\Ajax\BootstrapToastMessagesCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event Subscriber MessagesEventSubscriber.
 */
class MessagesEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {

    return [
      KernelEvents::RESPONSE => [['onKernelResponse']],
    ];

  }

  /**
   * Adds the bootstrap toast messages on the page.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event.
   */
  public function onKernelResponse(ResponseEvent $event) {
    $response = $event->getResponse();

    // Using private temp store to get the status messages provided by the
    // mymodule_preprocess_status_messages.
    $tempstore = \Drupal::service('tempstore.private')->get('bootstrap_toast_messages');

    // Only for AJAX responses. Cancel if not ajax.
    if (!$response instanceof AjaxResponse) {
      // Delete tempstore variable.
      $tempstore->delete('messages_array');
      return;
    }

    // Array of messages.
    $messagesArray = $tempstore->get('messages_array');

    // Have we set messages to be displayed?
    if (isset($messagesArray) && !empty($messagesArray)) {

      // Yes we have messages, so let's call the javascript command function
      // to display them.
      $response->addCommand(
        new BootstrapToastMessagesCommand($messagesArray)
      );

      // Delete tempstore variable.
      $tempstore->delete('messages_array');

    }

    // Let's do this...
    $event->setResponse($response);
  }

}
