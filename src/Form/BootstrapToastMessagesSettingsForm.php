<?php

namespace Drupal\bootstrap_toast_messages\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Bootstrap Toast Messages general settings.
 */
class BootstrapToastMessagesSettingsForm extends ConfigFormBase {

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ThemeHandlerInterface $theme_handler) {
    parent::__construct($config_factory);
    $this->themeHandler = $theme_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('theme_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bootstrap_toast_messages_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bootstrap_toast_messages.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bootstrap_toast_messages.settings');

    $form['toastjs_duration'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bootstrap Toast Message duration'),
      '#default_value' => $config->get('toastjs_duration'),
      '#size' => 40,
      '#description' => $this->t('The length of time, in milliseconds, the alert will show before closing itself.'),
    ];

    $form['toastjs_themes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Themes'),
      '#description' => $this->t('Themes to apply bootstrap toast messages.'),
      '#options' => $this->getThemeOptions(),
      '#default_value' => $config->get('toastjs_themes') ?: [$this->themeHandler->getDefault()],
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('bootstrap_toast_messages.settings')
      ->set('toastjs_duration', $form_state->getValue('toastjs_duration'))
      ->set('toastjs_themes', $form_state->getValue('toastjs_themes'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Return an array with all the themes.
   *
   * @return array
   *   An array with all the themes.
   */
  protected function getThemeOptions() {
    foreach ($this->themeHandler->listInfo() as $key => $value) {
      $output[$key] = $value->getName();
    }
    return $output;
  }

}
